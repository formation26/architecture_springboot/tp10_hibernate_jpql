package fr.epita.tp10.infra;

import java.util.List;

import fr.epita.tp10.domaine.Produit;

public interface DaoProduit {
	
	List<Produit> findAllProducts();
	
	List<Produit> findAllExpensiveProduct(double limitPrix);
	
	Produit getProduct(int id);
	
	void create(Produit p);
	
	void remove(Produit p);
	
	void update(Produit p);

}
