package fr.epita.tp10;

import java.util.List;

import fr.epita.tp10.application.ProduitService;
import fr.epita.tp10.application.ProduitServiceImpl;
import fr.epita.tp10.domaine.Produit;

public class App {

	public static void main(String[] args) {
		
		ProduitService service=new ProduitServiceImpl();
		
		Produit souris=new Produit();
		souris.setNom("souris sans fil");
		souris.setPrixTTC(30.00);
		service.create(souris);
		
		Produit clavier=new Produit();
		clavier.setNom("clavier sans fil");
		clavier.setPrixTTC(120.00);
		service.create(clavier);
		
		Produit ecran=new Produit();
		ecran.setNom("ecran 24 pouces");
		ecran.setPrixTTC(200.00);
		service.create(ecran);
		
		Produit tapisSouris=new Produit();
		tapisSouris.setNom("tapis gamer");
		tapisSouris.setPrixTTC(20.00);
		service.create(tapisSouris);
		
		
		List<Produit> expensive=service.findAllExpensiveProduct(150.00);
		System.out.println("-----------------Produit les plus chers-------------");
		for(Produit p : expensive) {
			
			System.out.println("Nom du produit "+p.getNom());
		}
		System.out.println("-----------------------------------------------------");
      
		
		List<Produit> allProducts=service.findAllProducts();
		System.out.println("-----------------List des produits-------------");
		for(Produit p : allProducts) {
			
			System.out.println("Nom du produit "+p.getNom());
		}
		System.out.println("-----------------------------------------------------");
	}

}
