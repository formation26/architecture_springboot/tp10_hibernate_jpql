package fr.epita.tp10.application;

import java.util.List;

import fr.epita.tp10.domaine.Produit;
import fr.epita.tp10.infra.DaoProduit;
import fr.epita.tp10.infra.Daofactory;

public class ProduitServiceImpl implements ProduitService {

	DaoProduit dao=Daofactory.createDaoproduit();
	
	@Override
	public List<Produit> findAllProducts() {
		
		return dao.findAllProducts();
	}

	@Override
	public List<Produit> findAllExpensiveProduct(double limitPrix) {
		
		return dao.findAllExpensiveProduct(limitPrix);
	}

	@Override
	public Produit getProduct(int id) {
		
		return dao.getProduct(id);
	}

	@Override
	public void create(Produit p) {
		//contrôle métiers
		dao.create(p);

	}

	@Override
	public void remove(Produit p) {
		//Contôle métiers
		dao.remove(p);

	}

	@Override
	public void update(Produit p) {
		//Contrôle métiers
		dao.update(p);
	}

}
