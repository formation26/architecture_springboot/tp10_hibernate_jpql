package fr.epita.tp10.application;

import java.util.List;

import fr.epita.tp10.domaine.Produit;

public interface ProduitService {
	

	List<Produit> findAllProducts();
	
	List<Produit> findAllExpensiveProduct(double limitPrix);
	
	Produit getProduct(int id);
	
	void create(Produit p);
	
	void remove(Produit p);
	
	void update(Produit p);

}
